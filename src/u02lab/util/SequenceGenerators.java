package u02lab.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * Created by nicolalasagni on 07/03/2017.
 */
public class SequenceGenerators {

    public static Optional<Integer> getNext(Iterator<Integer> integerIterator) {
        return integerIterator.hasNext() ? Optional.of(integerIterator.next()) : Optional.empty();
    }

    public static List<Integer> getAllRemaining(Iterator<Integer> integerIterator) {
        List<Integer> remainingBits = new ArrayList<>();
        while (integerIterator.hasNext()) {
            remainingBits.add(integerIterator.next());
        }
        return remainingBits;
    }

}
