package u02lab.code;

import u02lab.util.SequenceGenerators;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop has been produced, lead to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and gives false, at the end and gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator implements SequenceGenerator {

    private List<Integer> rangeList;
    private Iterator<Integer> iterator;

    public RangeGenerator(int start, int stop) {
        this.rangeList = IntStream.rangeClosed(start, stop).boxed().collect(Collectors.toList());
        this.iterator = rangeList.iterator();
    }

    @Override
    public Optional<Integer> next() {
        return SequenceGenerators.getNext(this.iterator);
    }

    @Override
    public void reset() {
        this.iterator = this.rangeList.iterator();
    }

    @Override
    public boolean isOver() {
        return !iterator.hasNext();
    }

    @Override
    public List<Integer> allRemaining() {
        return SequenceGenerators.getAllRemaining(iterator);
    }

}
