package u02lab.code;

/**
 * Created by nicolalasagni on 07/03/2017.
 */
public class SequenceGeneratorFactory extends SimpleSequenceGeneratorFactory {

    @Override
    protected RangeGenerator createRangeGenerator(int start, int stop) {
        return new RangeGenerator(start, stop);
    }

    @Override
    protected RandomGenerator createRandomGenerator(int randomCount) {
        return new RandomGenerator(randomCount);
    }

}
