package u02lab.code;

/**
 * Created by nicolalasagni on 07/03/2017.
 */
public abstract class SimpleSequenceGeneratorFactory {

    protected abstract SequenceGenerator createRangeGenerator(int start, int stop);

    protected abstract SequenceGenerator createRandomGenerator(int randomCount);

}
