package u02lab.code;

import u02lab.util.SequenceGenerators;

import java.util.*;
import java.util.stream.IntStream;

/**
 * Step 2
 *
 * Forget about class u02lab.code.RangeGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (u02lab.code.RandomGenerator vs u02lab.code.RangeGenerator)?
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator implements SequenceGenerator {

    private List<Integer> bitList;
    private Iterator<Integer> iterator;

    public RandomGenerator(int n) {
        this.bitList = new ArrayList<>();
        IntStream.range(0, n).boxed().forEach(integer -> this.bitList.add((int) Math.round(Math.random())));
        this.iterator = this.bitList.listIterator();
    }

    @Override
    public Optional<Integer> next() {
        return SequenceGenerators.getNext(this.iterator);
    }

    @Override
    public void reset() {
        this.iterator = this.bitList.iterator();
    }

    @Override
    public boolean isOver() {
        return !iterator.hasNext();
    }

    @Override
    public List<Integer> allRemaining() {
        return SequenceGenerators.getAllRemaining(iterator);
    }

}
