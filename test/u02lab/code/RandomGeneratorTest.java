package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.Optional;
import java.util.stream.IntStream;

import static org.junit.Assert.assertTrue;

/**
 * Created by nicolalasagni on 07/03/2017.
 */
public class RandomGeneratorTest {

    private static final SimpleSequenceGeneratorFactory FACTORY = new SequenceGeneratorFactory();
    private static final int BIT_0 = 0;
    private static final int BIT_1 = 1;
    private static final int BIT_COUNT = 5;
    private static final int ALL_REMAINING_START = 2;
    private static final int ALL_REMAINING_COUNT = BIT_COUNT - ALL_REMAINING_START;
    private SequenceGenerator randomGenerator;

    @Before
    public void init() {
        randomGenerator = FACTORY.createRandomGenerator(BIT_COUNT);
    }

    @Test
    public void next() {
        assertTrue(checkAllElementsInRange());
    }

    private boolean checkAllElementsInRange() {
        return IntStream.range(0, BIT_COUNT)
                .boxed()
                .map(integer -> randomGenerator.next())
                .allMatch(optional -> optional.isPresent() && isInRange(optional.get()));
    }

    @Test
    public void nextAfterStop() {
        assertTrue(SequenceGeneratorTests.iterateAllAndCheckEmptyElementAfterLast(randomGenerator, BIT_COUNT));
    }

    @Test
    public void reset() {
        SequenceGeneratorTests.iterateAllAndReset(randomGenerator, BIT_COUNT);
        Optional<Integer> nextAfterReset = randomGenerator.next();
        assertTrue(nextAfterReset.isPresent() && isInRange(nextAfterReset.get()));
    }

    @Test
    public void isOverInTheMiddle() {
        randomGenerator.next();
        assertTrue(!randomGenerator.isOver());
    }

    @Test
    public void isOver() {
        SequenceGeneratorTests.iterateAllNumbers(randomGenerator, BIT_COUNT);
        assertTrue(randomGenerator.isOver());
    }

    @Test
    public void allRemaining() {
        for (int i = 0; i < ALL_REMAINING_COUNT; i++) {
            randomGenerator.next();
        }
        randomGenerator.allRemaining().forEach(integer -> assertTrue(isInRange(integer)));
    }

    private boolean isInRange(Integer integer) {
        return integer == BIT_0 || integer == BIT_1;
    }

}