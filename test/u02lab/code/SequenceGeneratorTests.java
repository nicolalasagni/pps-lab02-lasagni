package u02lab.code;

import java.util.Optional;
import java.util.stream.IntStream;

/**
 * Created by nicolalasagni on 07/03/2017.
 */
class SequenceGeneratorTests {

    static boolean iterateAllAndCheckEmptyElementAfterLast(SequenceGenerator sequenceGenerator, int count) {
        iterateAllNumbers(sequenceGenerator, count);
        return sequenceGenerator.next().equals(Optional.empty());
    }

    static void iterateAllAndReset(SequenceGenerator sequenceGenerator, int count) {
        iterateAllNumbers(sequenceGenerator, count);
        sequenceGenerator.reset();
    }

    static void iterateAllNumbers(SequenceGenerator sequenceGenerator, int count) {
        IntStream.range(0, count).forEach(integer -> sequenceGenerator.next());
    }

}
