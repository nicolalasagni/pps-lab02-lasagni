package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by nicolalasagni on 07/03/2017.
 */
public class RangeGeneratorTest {

    private static final SimpleSequenceGeneratorFactory FACTORY = new SequenceGeneratorFactory();
    private static final Integer[] TEST_ARRAY = {0, 1, 2, 3, 4};
    private static final int TEST_ARRAY_LENGTH = TEST_ARRAY.length;
    private static final int START = 0;
    private static final int STOP = TEST_ARRAY_LENGTH - 1;
    private static final int ALL_REMAINING_START = 2;
    private static final int ALL_REMAINING_COUNT = TEST_ARRAY_LENGTH - ALL_REMAINING_START;
    private SequenceGenerator rangeGenerator;

    @Before
    public void init() {
        this.rangeGenerator = FACTORY.createRangeGenerator(START, STOP);
    }

    @Test
    public void next() {
        Integer[] nextArray = new Integer[TEST_ARRAY.length];
        for (int i = 0; i < TEST_ARRAY.length; i++) {
            Optional<Integer> next = rangeGenerator.next();
            if (next.isPresent()) {
                nextArray[i] = next.get();
            }
        }
        assertArrayEquals(TEST_ARRAY, nextArray);
    }

    @Test
    public void nextAfterStop() {
        assertTrue(SequenceGeneratorTests.iterateAllAndCheckEmptyElementAfterLast(rangeGenerator, TEST_ARRAY_LENGTH));
    }

    @Test
    public void reset() {
        SequenceGeneratorTests.iterateAllAndReset(rangeGenerator, TEST_ARRAY_LENGTH);
        assertEquals(Optional.of(START), rangeGenerator.next());
    }

    @Test
    public void isOverInTheMiddle() {
        rangeGenerator.next();
        assertTrue(!rangeGenerator.isOver());
    }

    @Test
    public void isOver() {
        SequenceGeneratorTests.iterateAllNumbers(rangeGenerator, TEST_ARRAY_LENGTH);
        assertTrue(rangeGenerator.isOver());
    }

    @Test
    public void allRemaining() {
        for (int i = 0; i < ALL_REMAINING_COUNT; i++) {
            rangeGenerator.next();
        }
        List<Integer> testList = Arrays.asList(TEST_ARRAY).subList(ALL_REMAINING_COUNT, TEST_ARRAY_LENGTH);
        assertArrayEquals(testList.toArray(), rangeGenerator.allRemaining().toArray());
    }

}